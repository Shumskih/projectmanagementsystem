require './init_db_connection'

class ProjectDao
  @@db_connection = InitDbConnection.init_db

  def save(project)
    @conn = @@db_connection.db_connection

    @conn.exec "INSERT INTO projects VALUES(#{project.id},'#{project.name}', '#{project.version}', #{project.cost})"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def get_by_id(project_id)
    @conn = @@db_connection.db_connection

    rs = @conn.exec "SELECT * FROM projects WHERE id = #{project_id}"

    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def get_by_name(project_name)
    @conn = @@db_connection.db_connection

    rs = @conn.exec "SELECT * FROM projects WHERE name = '#{project_name}'"

    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def show_all
    @conn = @@db_connection.db_connection

    rs = @conn.exec 'SELECT * FROM projects'

    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def update(project)
    @conn = @@db_connection.db_connection

    @conn.exec "UPDATE projects SET name = '#{project.name}', version = '#{project.version}', cost = #{project.cost}  WHERE id = #{project.id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def delete(project_id)
    @conn = @@db_connection.db_connection

    @conn.exec "DELETE FROM projects WHERE id = #{project_id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def delete_companies_projects(project_id)
    @conn = @@db_connection.db_connection

    @conn.exec "DELETE FROM companies_projects WHERE project_id = #{project_id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def delete_customers_projects(project_id)
    @conn = @@db_connection.db_connection

    @conn.exec "DELETE FROM customers_projects WHERE project_id = #{project_id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def delete_developers_projects(project_id)
    @conn = @@db_connection.db_connection

    @conn.exec "DELETE FROM developers_projects WHERE project_id = #{project_id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end
end