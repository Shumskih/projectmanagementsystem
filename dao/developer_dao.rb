require './init_db_connection'

class DeveloperDao
  @@db_connection = InitDbConnection.init_db

  def save(developer)
    @conn = @@db_connection.db_connection

    @conn.exec "INSERT INTO developers VALUES(#{developer.id},'#{developer.name}','#{developer.surname}','#{developer.specialization}',#{developer.experience},#{developer.salary})"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def get_by_id(developer_id)
    @conn = @@db_connection.db_connection

    rs = @conn.exec "SELECT * FROM developers WHERE id = #{developer_id}"

    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def get_by_name(developer_name)
    @conn = @@db_connection.db_connection

    rs = @conn.exec "SELECT * FROM developers WHERE name = '#{developer_name}'"

    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def show_all
    @conn = @@db_connection.db_connection

    rs = @conn.exec 'SELECT * FROM developers'

    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def update(developer)
    @conn = @@db_connection.db_connection

    @conn.exec "UPDATE developers SET name = '#{developer.name}', surname = '#{developer.surname}',
                specialization = '#{developer.specialization}', experience = #{developer.experience},
                salary = #{developer.salary}
                WHERE id = #{developer.id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def delete(developer_id)
    @conn = @@db_connection.db_connection

    @conn.exec "DELETE FROM developers WHERE id = #{developer_id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def developers_skills(developer_id, skill_id)
    @conn = @@db_connection.db_connection

    @conn.exec "INSERT INTO developers_skills VALUES(#{developer_id},'#{skill_id}')"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def delete_skills_developers(skill_id)
    @conn = @@db_connection.db_connection

    @conn.exec "DELETE FROM developers_skills WHERE skill_id = #{skill_id}"

    @@db_connection.close_db_connection(@conn)
  end

  def delete_developers_skills(developer_id)
    @conn = @@db_connection.db_connection

    @conn.exec "DELETE FROM developers_skills WHERE developer_id = #{developer_id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def developers_projects(developer_id, project_id)
    @conn = @@db_connection.db_connection

    @conn.exec "INSERT INTO developers_projects VALUES(#{developer_id},'#{project_id}')"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def delete_projects_developers(project_id)
    @conn = @@db_connection.db_connection

    @conn.exec "DELETE FROM developers_projects WHERE project_id = #{project_id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def delete_developers_projects(developer_id)
    @conn = @@db_connection.db_connection

    @conn.exec "DELETE FROM developers_projects WHERE developer_id = #{developer_id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def show_all_skills_by_dev_id(developer_id)
    @conn = @@db_connection.db_connection

    rs = @conn.exec "SELECT skill_id FROM developers_skills WHERE developer_id = #{developer_id}"

    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def show_absent_skills(existing_skill_ids)
    @conn = @@db_connection.db_connection

    @id = existing_skill_ids[0]
    @expression = "SELECT * FROM skills WHERE id != #{@id}"
    length = existing_skill_ids.length
    existing_skill_ids.delete_at(0)

    if length < 1
      return nil
    elsif length >= 1
      existing_skill_ids.each do |id|
        @expression += " AND id != #{id}"
      end
    end

    rs = @conn.exec @expression

    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def show_all_projects_by_dev_id(developer_id)
    @conn = @@db_connection.db_connection

    rs = @conn.exec "SELECT project_id FROM developers_projects WHERE developer_id = #{developer_id}"

    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def show_absent_projects(existing_project_ids)
    @conn = @@db_connection.db_connection

    @id = existing_project_ids[0]
    @expression = "SELECT * FROM projects WHERE id != #{@id}"
    length = existing_project_ids.length
    existing_project_ids.delete_at(0)

    if length < 1
      return nil
    elsif length >= 1
      existing_project_ids.each do |id|
        @expression += " AND id != #{id}"
      end
    end

    rs = @conn.exec @expression

    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end
end