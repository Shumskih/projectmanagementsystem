require './init_db_connection'

class CompanyDao
  @@db_connection = InitDbConnection.init_db

  def save(company)
    @conn = @@db_connection.db_connection

    @conn.exec "INSERT INTO companies VALUES(#{company.id},'#{company.name}')"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def get_by_id(company_id)
    @conn = @@db_connection.db_connection

    rs = @conn.exec "SELECT * FROM companies WHERE id = #{company_id}"

    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def get_by_name(company_name)
    @conn = @@db_connection.db_connection

    rs = @conn.exec "SELECT * FROM companies WHERE name = '#{company_name}'"

    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def show_all
    @conn = @@db_connection.db_connection

    rs = @conn.exec 'SELECT * FROM companies'
    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def update(company)
    @conn = @@db_connection.db_connection

    @conn.exec "UPDATE companies SET name = '#{company.name}'  WHERE id = #{company.id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def delete(company_id)
    @conn = @@db_connection.db_connection

    @conn.exec "DELETE FROM companies WHERE id = #{company_id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def delete_company_projects(company_id)
    @conn = @@db_connection.db_connection

    @conn.exec "DELETE FROM companies_projects WHERE company_id = #{company_id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def companies_projects(company_id, project_id)
    @conn = @@db_connection.db_connection

    @conn.exec "INSERT INTO companies_projects VALUES(#{company_id},'#{project_id}')"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def show_all_projects_by_com_id(company_id)
    @conn = @@db_connection.db_connection

    rs = @conn.exec "SELECT project_id FROM companies_projects WHERE company_id = #{company_id}"

    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def show_absent_projects(existing_project_ids)
    @conn = @@db_connection.db_connection

    @id = existing_project_ids[0]
    @expression = "SELECT * FROM projects WHERE id != #{@id}"
    length = existing_project_ids.length
    existing_project_ids.delete_at(0)

    if length < 1
      return nil
    elsif length >= 1
      existing_project_ids.each do |id|
        @expression += " AND id != #{id}"
      end
    end

    rs = @conn.exec @expression

    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def delete_projects_companies(project_id)
    @conn = @@db_connection.db_connection

    @conn.exec "DELETE FROM companies_projects WHERE project_id = #{project_id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def delete_companies_projects(company_id)
    @conn = @@db_connection.db_connection

    @conn.exec "DELETE FROM companies_projects WHERE company_id = #{company_id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end
end