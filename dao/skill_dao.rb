require './init_db_connection'

class SkillDao
  @@db_connection = InitDbConnection.init_db

  def save(skill)
    @conn = @@db_connection.db_connection

    @conn.exec "INSERT INTO skills VALUES(#{skill.id},'#{skill.name}')"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def get_by_id(skill_id)
    @conn = @@db_connection.db_connection

    rs = @conn.exec "SELECT id, name FROM skills WHERE id = #{skill_id}"

    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def get_by_name(skill_name)
    @conn = @@db_connection.db_connection

    rs = @conn.exec "SELECT id, name FROM skills WHERE name = '#{skill_name}'"

    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def show_all
    @conn = @@db_connection.db_connection

    rs = @conn.exec 'SELECT * FROM skills'

    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def update(skill)
    @conn = @@db_connection.db_connection

    @conn.exec "UPDATE skills SET name = '#{skill.name}'  WHERE id = #{skill.id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def delete(skill_id)
    @conn = @@db_connection.db_connection

    @conn.exec "DELETE FROM skills WHERE id = #{skill_id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def delete_skill_developer(skill_id)
    @conn = @@db_connection.db_connection

    @conn.exec "DELETE FROM developers_skills WHERE skill_id = #{skill_id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end
end
