require './init_db_connection'

class CustomerDao
  @@db_connection = InitDbConnection.init_db

  def save(customer)
    @conn = @@db_connection.db_connection

    @conn.exec "INSERT INTO customers VALUES(#{customer.id},'#{customer.name}')"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def get_by_id(customer_id)
    @conn = @@db_connection.db_connection

    rs = @conn.exec "SELECT * FROM customers WHERE id = #{customer_id}"

    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def get_by_name(customer_name)
    @conn = @@db_connection.db_connection

    rs = @conn.exec "SELECT * FROM customers WHERE name = '#{customer_name}'"

    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def show_all
    @conn = @@db_connection.db_connection

    rs = @conn.exec 'SELECT * FROM customers'

    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def update(customer)
    @conn = @@db_connection.db_connection

    @conn.exec "UPDATE customers SET name = '#{customer.name}' WHERE id = #{customer.id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def delete(customer_id)
    @conn = @@db_connection.db_connection

    @conn.exec "DELETE FROM customers WHERE id = #{customer_id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def delete_customer_projects(customer_id)
    @conn = @@db_connection.db_connection

    @conn.exec "DELETE FROM customers_projects WHERE customer_id = #{customer_id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def customers_projects(customer_id, project_id)
    @conn = @@db_connection.db_connection

    @conn.exec "INSERT INTO customers_projects VALUES(#{customer_id},'#{project_id}')"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def show_all_projects_by_cus_id(customer_id)
    @conn = @@db_connection.db_connection

    rs = @conn.exec "SELECT project_id FROM customers_projects WHERE customer_id = #{customer_id}"

    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def show_absent_projects(existing_project_ids)
    @conn = @@db_connection.db_connection

    @id = existing_project_ids[0]
    @expression = "SELECT * FROM projects WHERE id != #{@id}"
    length = existing_project_ids.length
    existing_project_ids.delete_at(0)

    if length < 1
      return nil
    elsif length >= 1
      existing_project_ids.each do |id|
        @expression += " AND id != #{id}"
      end
    end

    rs = @conn.exec @expression

    return rs if rs.ntuples > 0
    nil if rs.ntuples.zero?
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def delete_projects_customers(project_id)
    @conn = @@db_connection.db_connection

    @conn.exec "DELETE FROM customers_projects WHERE project_id = #{project_id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end

  def delete_customers_projects(customer_id)
    @conn = @@db_connection.db_connection

    @conn.exec "DELETE FROM customers_projects WHERE customer_id = #{customer_id}"
  rescue PG::Error => e
    puts e.message
  ensure
    @@db_connection.close_db_connection(@conn) if @conn
  end
end