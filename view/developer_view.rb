require './model/developer'
require './controller/developer_controller'
require './controller/skill_controller'
require './controller/project_controller'
require './common/decorations/decor'
require_relative 'console_helper'
require_relative 'item_view'

class DeveloperView
  @@developer_controller = DeveloperController.new
  @@skill_controller = SkillController.new
  @@project_controller = ProjectController.new
  @@item_view = ItemView.new

  @@id = nil
  @@name = nil
  @@surname = nil
  @@specialization = nil
  @@experience = nil
  @@salary = nil

  def create
    @exit = false
    create_developer until @exit
    @exit = true
  end

  def update
    @exit = false
    update_developer until @exit
    @exit = true
  end

  def get_by_id
    @exit = false

    until @exit
      puts 'Enter developer id or c to cancel:'
      user_input = gets.chomp.strip

      if user_input == 'c'
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif !Common.numeric?(user_input)
        puts 'ID must be integer!'
        puts
        next
      else
        developer_id = user_input
        id_exists = id_exists?(developer_id)
        if id_exists
          show_developer_by_id(developer_id)
        else
          puts "No developer with id = #{developer_id}"
          puts
        end
      end
    end
  end

  def show_all
    @exit = false

    until @exit
      show_all_developers
      puts
      puts 'Back to main menu - enter m'
      user_input = gets.strip
      @exit = true if user_input.include?('m') || user_input.include?('') || user_input.include?('\n')
      ConsoleHelper.console_helper
    end
    @exit = true
  end

  def show_all_developers
    developers = @@developer_controller.show_all
    if developers.nil?
      puts '----- ' + 'No developers yet' + ' -----'
    else
      developers.each do |developer|
        puts '===================='
        puts format('ID: %s', developer['id'])
        puts format('Name: %s', developer['name'])
        puts format('Surname: %s', developer['surname'])
        puts format('Specialization: %s', developer['specialization'])
        puts format('Experience: %s', developer['experience'])
        puts format('Salary: %s', developer['salary'])
        puts '---'
        puts 'Skills: '
        skills_ids = @@developer_controller.show_all_skills_by_dev_id(developer['id'])
        if skills_ids.nil?
          puts '         -- No skills --'
          puts
        else
          skills_ids.each do |skill|
            skill_id = skill['skill_id']
            skill = @@skill_controller.get_by_id(skill_id)
            puts format('         ID: %s', skill[0]['id'])
            puts format('         Name: %s', skill[0]['name'])
            puts '         -'
          end
          skills_ids.clear
        end
        puts '---'
        puts 'Projects: '
        projects_ids = @@developer_controller.show_all_projects_by_dev_id(developer['id'])
        if projects_ids.nil?
          puts '         -- No projects --'
          puts
        else
          projects_ids.each do |project|
            project_id = project['project_id']
            project = @@project_controller.get_by_id(project_id)
            puts format('         ID: %s', project[0]['id'])
            puts format('         Name: %s', project[0]['name'])
            puts format('         Version: %s', project[0]['version'])
            puts format('         Cost: %s', project[0]['cost'])
            puts '         -'
          end
          projects_ids.clear
        end
      end
      developers.clear
    end
  end

  def show_developer_by_id(developer_id)
    developer = @@developer_controller.get_by_id(developer_id)
    if developer.nil?
      puts '----- ' + 'No developer' + ' -----'
    else
      developer.each do |row|
        puts '===================='
        puts format('ID: %s', row['id'])
        puts format('Name: %s', row['name'])
        puts format('Surname: %s', row['surname'])
        puts format('Specialization: %s', row['specialization'])
        puts format('Experience: %s', row['experience'])
        puts format('Salary: %s', row['salary'])
        puts '---'
        puts 'Skills: '
        skills_ids = @@developer_controller.show_all_skills_by_dev_id(row['id'])
        if skills_ids.nil?
          puts '         -- No skills --'
          puts
        else
          skills_ids.each do |skill|
            skill_id = skill['skill_id']
            skill = @@skill_controller.get_by_id(skill_id)
            puts format('         ID: %s', skill[0]['id'])
            puts format('         Name: %s', skill[0]['name'])
            puts '         -'
          end
          skills_ids.clear
        end
        puts '---'
        puts 'Projects: '
        projects_ids = @@developer_controller.show_all_projects_by_dev_id(row['id'])
        if projects_ids.nil?
          puts '         -- No projects --'
          puts
        else
          projects_ids.each do |project|
            project_id = project['project_id']
            project = @@project_controller.get_by_id(project_id)
            puts format('         ID: %s', project[0]['id'])
            puts format('         Name: %s', project[0]['name'])
            puts format('         Version: %s', project[0]['version'])
            puts format('         Cost: %s', project[0]['cost'])
            puts '         -'
          end
          projects_ids.clear
        end
      end
      developer.clear
    end
  end

  def delete
    @exit = false

    puts 'Searching existing developers:'
    Decor.run
    puts
    show_all_developers
    puts

    until @exit
      puts 'Enter developer id or c to cancel:'
      user_input = gets.chomp.strip

      if user_input == 'c'
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif !Common.numeric?(user_input)
        puts 'ID must be integer!'
        puts
        next
      else
        developer_id = user_input
        id_exists = id_exists?(developer_id)
        if id_exists
          @@developer_controller.delete_developers_projects(user_input)
          @@developer_controller.delete_developers_skills(user_input)
          @@developer_controller.delete(user_input)
          id_exists = id_exists?(developer_id)
          puts
          puts "Developer with id = #{developer_id} has successfully deleted" unless id_exists
          puts
        else
          puts "No developer with id = #{developer_id}"
          puts
          next
        end
      end
    end
    @exit = true
  end

  def create_developer
    @exit = false
    @item = 'developer'

    puts 'Searching existing developers:'
    Decor.run
    puts
    show_all_developers
    puts

    until @exit
      @@id = @@item_view.item_view(@item, 'id')
      if @@id == true
        @exit = true
        break
      end
      id_exists = id_exists?(@@id)
      if id_exists
        puts "ID #{@@id} is already exists. Please, choose another ID."
        puts
        next
      else
        break
      end
    end

    until @exit
      @@name = @@item_view.item_view(@item, 'name')
      if @@name == true
        @exit = true
        break
      end
      name_exists = name_exists?(@@name)
      if name_exists
        puts "Name '#{@@name}' is already exists. Please, choose another name."
        puts
        next
      else
        break
      end
    end

    until @exit
      @@surname = @@item_view.item_view(@item, 'surname')
      @exit = true if @@surname == true
      break
    end

    until @exit
      @@specialization = @@item_view.item_view(@item, 'specialization')
      @exit = true if @@specialization == true
      break
    end

    until @exit
      @@experience = @@item_view.item_view(@item, 'experience')
      @exit = true if @@experience == true
      break
    end

    until @exit
      @@salary = @@item_view.item_view(@item, 'salary')
      @exit = true if @@salary == true
      break
    end

    until @exit
      developer = Developer.new
      developer.id = @@id
      developer.name = @@name
      developer.surname = @@surname
      developer.specialization = @@specialization
      developer.experience = @@experience
      developer.salary = @@salary

      @@developer_controller.save(developer)

      id_exists = id_exists?(@@id)
      puts
      puts "Developer #{@@name} with id = #{@@id} has saved" if id_exists
      puts
      break
    end

    until @exit
      puts 'Add skill to developer? Yes(y) or No(n):'
      user_input = gets.chomp.strip

      if user_input.include?('n') || user_input.include?('no')
        break
      elsif user_input == ''
        next
      elsif user_input.include?('y') || user_input.include?('yes')
        add_skill(@@id)
        break
      end
    end

    until @exit
      puts 'Add project to developer? Yes(y) or No(n):'
      user_input = gets.chomp.strip

      if user_input.include?('n') || user_input.include?('no')
        break
      elsif user_input == ''
        next
      elsif user_input.include?('y') || user_input.include?('yes')
        add_project(@@id)
        break
      end
    end

    until @exit
      puts 'Create another one developer? Yes(y) or No(n):'
      user_input = gets.chomp.strip

      if user_input.include?('n') || user_input.include?('no')
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif user_input == ''
        next
      elsif user_input.include?('y') || user_input.include?('yes')
        break
      end
    end
    @@id = nil
    @@name = nil
    @@surname = nil
    @@specialization = nil
    @@experience = nil
    @@salary = nil
  end

  def update_developer
    @exit = false
    @item = 'developer'
    @developer_id = nil

    puts 'Searching existing developers:'
    Decor.run
    puts
    show_all_developers
    puts
    puts "Enter developer id you're going to update or c to cancel:"
    @user_input = gets.chomp.strip

    until @exit
      if @user_input == 'c'
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif !Common.numeric?(@user_input)
        puts 'ID must be integer!'
        puts
        next
      else
        @developer_id = @user_input
        @old_developer = @@developer_controller.get_by_id(@user_input)
        @developer_id = @user_input
        if @old_developer.nil?
          puts "No developer with id = #{@user_input}"
        else
          show_developer_by_id(@developer_id)
          break
        end
      end
    end

    until @exit
      puts 'Change: '
      puts '    1.name?'
      puts '    2.surname?'
      puts '    3.specialization?'
      puts '    4.experience?'
      puts '    5.salary?'
      puts '    6.skill?'
      puts '    7.project?'
      puts '8.Cancel'

      @user_input = gets.chomp.strip

      if @user_input == '8'
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif !@user_input =~ /[1-7]/
        puts 'Enter 1-7 to change something or 8 to cancel.'
        puts
      else
        case @user_input
        when '1'
          @user_input = @@item_view.item_view(@item, 'name')

          new_developer = Developer.new
          new_developer.id = @old_developer[0]['id']
          new_developer.name = @user_input
          new_developer.surname = @old_developer[0]['surname']
          new_developer.specialization = @old_developer[0]['specialization']
          new_developer.experience = @old_developer[0]['experience']
          new_developer.salary = @old_developer[0]['salary']

          @@developer_controller.update(new_developer)
          puts "Name #{@old_developer[0]['name']} have changed to #{@user_input}"
          @old_developer.clear
          next
        when '2'
          @user_input = @@item_view.item_view(@item, 'surname')

          new_developer = Developer.new
          new_developer.id = @old_developer[0]['id']
          new_developer.name = @old_developer[0]['name']
          new_developer.surname = @user_input
          new_developer.specialization = @old_developer[0]['specialization']
          new_developer.experience = @old_developer[0]['experience']
          new_developer.salary = @old_developer[0]['salary']

          @@developer_controller.update(new_developer)
          puts "Surname #{@old_developer[0]['surname']} have changed to #{@user_input}"
          @old_developer.clear
          next
        when '3'
          @user_input = @@item_view.item_view(@item, 'specialization')

          new_developer = Developer.new
          new_developer.id = @old_developer[0]['id']
          new_developer.name = @old_developer[0]['name']
          new_developer.surname = @old_developer[0]['surname']
          new_developer.specialization = @user_input
          new_developer.experience = @old_developer[0]['experience']
          new_developer.salary = @old_developer[0]['salary']

          @@developer_controller.update(new_developer)
          puts "Specialization #{@old_developer[0]['specialization']} have changed to #{@user_input}"
          @old_developer.clear
          next
        when '4'
          @user_input = @@item_view.item_view(@item, 'experience')

          new_developer = Developer.new
          new_developer.id = @old_developer[0]['id']
          new_developer.name = @old_developer[0]['name']
          new_developer.surname = @old_developer[0]['surname']
          new_developer.specialization = @old_developer[0]['specialization']
          new_developer.experience = @user_input
          new_developer.salary = @old_developer[0]['salary']

          @@developer_controller.update(new_developer)
          puts "Experience #{@old_developer[0]['experience']} have changed to #{@user_input}"
          @old_developer.clear
          next
        when '5'
          @user_input = @@item_view.item_view(@item, 'salary')

          new_developer = Developer.new
          new_developer.id = @old_developer[0]['id']
          new_developer.name = @old_developer[0]['name']
          new_developer.surname = @old_developer[0]['surname']
          new_developer.specialization = @old_developer[0]['specialization']
          new_developer.experience = @old_developer[0]['experience']
          new_developer.salary = @user_input

          @@developer_controller.update(new_developer)
          puts "Salary #{@old_developer[0]['salary']} have changed to #{@user_input}"
          @old_developer.clear
          next
        when '6'
          change_skill(@developer_id)
          next
        when '7'
          change_project(@developer_id)
          next
        else
          puts 'Oooops'
        end
      end
    end
  end

  def change_skill(developer_id)
    exit = false

    until exit
      puts 'You want to:'
      puts '      1.add new skill to developer'
      puts '      2.delete skill from developer'
      puts '      3.back'

      user_input = gets.strip

      case user_input
      when '1'
        add_new_skill_to_developer(developer_id)
        exit = true
        break if exit
      when '2'
        delete_skill_from_developer(developer_id)
        exit = true
        break if exit
      else
        if !user_input =~ /[1-3]/
          puts 'Enter 1-3'
          next
        elsif user_input == '3'
          exit = true
        end
      end
    end
  end

  def add_new_skill_to_developer(developer_id)
    exit = false

    until exit
      puts 'Choose id of skill or c to cancel:'
      puts
      developer_skills = []
      skills_id = @@developer_controller.show_all_skills_by_dev_id(developer_id)

      if skills_id.nil?
        skills = @@skill_controller.show_all
        if skills.nil?
          puts 'No existing skills. Create skills first.'
          puts
          break
        else
          skills.each do |skill|
            puts '=========='
            puts 'ID: %s' % [skill['id']]
            puts 'Name: %s' % [skill['name']]
            puts
          end
        end
        user_input = gets.strip
        if user_input == 'c'
          exit = true
          break
        elsif !Common.numeric?(user_input)
          puts 'ID must be an integer!'
          puts
          next
        else
          add_absent_skill(developer_id, user_input)
          puts 'Skill added.'
          next
        end
      else
        skills_id.each do |id|
          developer_skills << id['skill_id']
        end

        absent_skills = @@developer_controller.show_absent_skills(developer_skills)

        absent_skills.each do |row|
          puts '================='
          puts 'ID: ' + row['id']
          puts 'Name: ' + row['name']
        end

        user_input = gets.strip
        if user_input == 'c'
          break
        elsif !Common.numeric?(user_input)
          puts 'ID must be integer!'
          puts
          next
        else
          if skill_exists?(developer_id, user_input)
            puts "Developer already have skill with id = #{user_input}"
            puts
            next
          else
            add_absent_skill(developer_id, user_input)
            exit = true
            next
          end
        end
      end
    end
  end

  def delete_skill_from_developer(developer_id)
    exit = false

    until exit
      ids_of_developer_skills = []
      skills_id = @@developer_controller.show_all_skills_by_dev_id(developer_id)
      if skills_id.nil?
        puts 'Developer have no skills. Nothing to delete...'
        puts
        exit = true
        break if exit
      else
        puts 'Skills of developer:'
        skills_id.each do |id|
          skill = @@skill_controller.get_by_id(id['skill_id'])
          ids_of_developer_skills << skill[0]['id']
          puts '============='
          puts 'ID: ' + skill[0]['id']
          puts 'Name: ' + skill[0]['name']
          puts
        end
      end
      puts 'Choose id of skill to delete or c to cancel:'

      user_input = gets.strip
      if user_input == 'c'
        break
      elsif !Common.numeric?(user_input)
        puts 'ID must be integer!'
        next
      else
        result = @@skill_controller.get_by_id(user_input)
        if !ids_of_developer_skills.include?(user_input)
          puts "Developer have no skill with id = #{user_input}"
          puts
        else
          skill_id = result[0]['id']
          @@developer_controller.delete_skills_developers(skill_id)
          puts 'Skill deleted'
          exit = true
          break
        end
      end
    end
  end

  def change_project(developer_id)
    exit = false

    until exit
      puts 'You want to:'
      puts '      1.add new project to developer'
      puts '      2.delete project from developer'
      puts '      3.back'

      user_input = gets.strip

      case user_input
      when '1'
        add_new_project_to_developer(developer_id)
        exit = true
        break if exit
      when '2'
        delete_project_from_developer(developer_id)
        exit = true
        break if exit
      else
        if !user_input =~ /[1-3]/
          puts 'Enter 1-3'
          next
        elsif user_input == '3'
          exit = true
        end
      end
    end
  end

  def add_new_project_to_developer(developer_id)
    exit = false

    until exit
      puts 'Choose id of project or c to cancel:'
      puts
      developer_projects = []
      projects_ids = @@developer_controller.show_all_projects_by_dev_id(developer_id)

      if projects_ids.nil?
        projects = @@project_controller.show_all
        if projects.nil?
          puts 'No existing projects. Create first.'
          puts
          break
        else
          projects.each do |project|
            puts '=========='
            puts 'ID: %s' % [project['id']]
            puts 'Name: %s' % [project['name']]
            puts 'Version: %s' % [project['version']]
            puts 'Cost: %s' % [project['cost']]
            puts
          end
        end
        user_input = gets.strip
        if user_input == 'c'
          break
        elsif !Common.numeric?(user_input)
          puts 'ID must be an integer!'
          puts
          next
        else
          add_absent_project(developer_id, user_input)
          puts 'Project added.'
          next
        end
      else
        projects_ids.each do |id|
          developer_projects << id['project_id']
        end

        absent_projects = @@developer_controller.show_absent_projects(developer_projects)

        absent_projects.each do |row|
          puts '================='
          puts 'ID: ' + row['id']
          puts 'Name: ' + row['name']
          puts 'Version: ' + row['version']
          puts 'Cost: ' + row['cost']
        end

        user_input = gets.strip
        if user_input == 'c'
          break
        elsif !Common.numeric?(user_input)
          puts 'ID must be integer!'
          puts
          next
        else
          if project_exists?(developer_id, user_input)
            puts "Developer already have project with id = #{user_input}"
            puts
            next
          else
            add_absent_project(developer_id, user_input)
            puts 'Project has added.'
            exit = true
            next
          end
        end
      end
    end
  end

  def delete_project_from_developer(developer_id)
    exit = false

    until exit
      ids_of_developer_projects = []
      projects_id = @@developer_controller.show_all_projects_by_dev_id(developer_id)
      if projects_id.nil?
        puts 'Developer have no projects. Nothing to delete...'
        puts
        break
      else
        puts 'Projects of developer:'
        projects_id.each do |id|
          project = @@project_controller.get_by_id(id['project_id'])
          ids_of_developer_projects << project[0]['id']
          puts '============='
          puts 'ID: ' + project[0]['id']
          puts 'Name: ' + project[0]['name']
          puts 'Version: ' + project[0]['version']
          puts 'Cost: ' + project[0]['cost']
          puts
        end
      end
      puts 'Choose id of project to delete or c to cancel:'

      user_input = gets.strip
      if user_input == 'c'
        break
      elsif !Common.numeric?(user_input)
        puts 'ID must be integer!'
        next
      else
        result = @@project_controller.get_by_id(user_input)
        if !ids_of_developer_projects.include?(user_input)
          puts "Developer have no project with id = #{user_input}"
          puts
        else
          project_id = result[0]['id']
          @@developer_controller.delete_projects_developers(project_id)
          puts 'Project deleted'
          break
        end
      end
    end
  end

  def add_absent_skill(developer_id, skill_id)
    exit = false

    until exit
      skill = @@skill_controller.get_by_id(skill_id)
      if skill.nil?
        puts "No skill with id = #{skill_id}"
        next
      else
        skill_id = skill[0]['id']
        @@developer_controller.developers_skills(developer_id, skill_id)
        exit = true
        break if exit
      end
    end
  end

  def add_absent_project(developer_id, project_id)
    exit = false

    until exit
      project = @@project_controller.get_by_id(project_id)
      if project.nil?
        puts "No project with id = #{project_id}"
      else
        project_id = project[0]['id']
        @@developer_controller.developers_projects(developer_id, project_id)
        break
      end
    end
  end

  def add_skill(developer_id)
    @exit = false

    skills = @@skill_controller.show_all
    skills.each do |skill|
      puts '=========='
      puts 'ID: %s' % skill['id']
      puts 'Name: %s' % skill['name']
      puts
    end

    until @exit
      puts 'Choose id of skill or c to cancel:'
      user_input = gets.strip
      skill = @@skill_controller.get_by_id(user_input)
      if skill.nil?
        puts "No skill with id = #{user_input}"
        puts
      elsif user_input == 'c'
        break
      elsif !Common.numeric?(user_input)
        puts 'ID must be numeric!'
        puts
        next
      else
        skill_id = skill[0]['id']
        @@developer_controller.developers_skills(developer_id, skill_id)
        puts 'Skill has added.'
        puts
      end

      puts 'Add another one skill? y = yes, n = no:'
      user_input = gets.chomp
      if user_input.include?('y') || user_input.include?('yes')
        next
      else
        break
      end
    end
  end

  def add_project(developer_id)
    @exit = false

    projects = @@project_controller.show_all
    projects.each do |project|
      puts '=========='
      puts 'ID: %s' % project['id']
      puts 'Name: %s' % project['name']
      puts 'Version: %s' % project['version']
      puts 'Cost: %s' % project['cost']
    end

    until @exit
      puts 'Choose id of project or c to cancel:'
      user_input = gets.strip
      project = @@project_controller.get_by_id(user_input)
      if project.nil?
        puts "No project with id = #{user_input}"
        puts
      elsif user_input == 'c'
        break
      elsif !Common.numeric?(user_input)
        puts 'ID must be numeric!'
        puts
        next
      else
        project_id = project[0]['id']
        @@developer_controller.developers_projects(developer_id, project_id)
        puts 'Project has added.'
        puts
      end

      puts 'Add another one project? y = yes, n = no:'
      user_input = gets.chomp
      if user_input.include?('y') || user_input.include?('yes')
        next
      else
        break
      end
    end
  end

  def id_exists?(developer_id)
    id = @@developer_controller.get_by_id(developer_id)

    return false if id.nil?
    true unless id.nil?
  end

  def name_exists?(developer_name)
    developer = @@developer_controller.get_by_name(developer_name)

    return false if developer.nil?
    true unless developer.nil?
  end

  def project_exists?(developer_id, project_id)
    projects_ids = @@developer_controller.show_all_projects_by_dev_id(developer_id)

    projects_ids.each do |row|
      if project_id == row['id']
        return true
      else
        return false
      end
    end
  end

  def skill_exists?(developer_id, skill_id)
    skills_ids = @@developer_controller.show_all_skills_by_dev_id(developer_id)

    skills_ids.each do |row|
      if skill_id == row['id']
        return true
      else
        return false
      end
    end
  end
end
