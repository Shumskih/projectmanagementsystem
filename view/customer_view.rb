require './controller/customer_controller'
require './controller/project_controller'
require './model/customer'
require './common/decorations/decor'
require './common/common'
require_relative 'console_helper'
require_relative 'item_view'

class CustomerView
  @@customer_controller = CustomerController.new
  @@project_controller = ProjectController.new
  @@item_view = ItemView.new

  @@id = nil
  @@name = nil

  def create
    @exit = false
    create_customer until @exit
    @exit = true
  end

  def update
    @exit = false
    update_customer until @exit
    @exit = true
  end

  def get_by_id
    @exit = false

    until @exit
      puts 'Enter customer id or c to cancel:'
      user_input = gets.chomp.strip

      if user_input == 'c'
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif !Common.numeric?(user_input)
        puts 'ID must be an integer!'
        puts
        next
      else
        id_exists = id_exists?(user_input)
        if id_exists
          customer = @@customer_controller.get_by_id(user_input)
          customer.each do |row|
            puts '===================='
            puts 'ID: %s' % [row['id']]
            puts 'Name: %s' % [row['name']]
            puts '---'
            puts 'Projects: '
            projects_ids = @@customer_controller.show_all_projects_by_cus_id(row['id'])
            if projects_ids.nil?
              puts '         -- No projects --'
              puts
            else
              projects_ids.each do |project|
                project_id = project['project_id']
                project = @@project_controller.get_by_id(project_id)
                puts '         ID: %s' % [project[0]['id']]
                puts '         Name: %s' % [project[0]['name']]
                puts '         Cost: %s' % [project[0]['cost']]
                puts '         Version: %s' % [project[0]['version']]
                puts '         -'
                puts
              end
              projects_ids.clear
            end
          end
          customer.clear
        else
          puts "There is no customer with id = #{user_input}"
          puts
        end
      end
    end
  end

  def show_all
    @exit = false

    until @exit
      show_all_customers
      puts
      puts 'Back to main menu - enter m'
      user_input = gets.strip
      @exit = true if user_input.include?('m') || user_input.include?('') || user_input.include?('\n')
      ConsoleHelper.console_helper
    end
    @exit = true
  end

  def show_all_customers
    customers = @@customer_controller.show_all
    if customers.nil?
      puts '----- ' + 'No customers yet' + ' -----'
    else
      customers.each do |customer|
        puts '===================='
        puts 'ID: %s' % [customer['id']]
        puts 'Name: %s' % [customer['name']]
        puts '---'
        puts 'Projects: '
        projects_ids = @@customer_controller.show_all_projects_by_cus_id(customer['id'])
        if projects_ids.nil?
          puts '         -- No projects --'
          puts
        else
          projects_ids.each do |project|
            project_id = project['project_id']
            project = @@project_controller.get_by_id(project_id)
            puts '         ID: %s' % [project[0]['id']]
            puts '         Name: %s' % [project[0]['name']]
            puts '         Cost: %s' % [project[0]['cost']]
            puts '         Version: %s' % [project[0]['version']]
            puts '         -'
          end
          projects_ids.clear
        end
      end
      customers.clear
    end
  end

  def delete
    @exit = false

    puts 'Searching existing customers:'
    Decor.run
    puts
    show_all_customers
    puts

    until @exit
      puts 'Enter customer id or c to cancel:'
      user_input = gets.chomp.strip

      if user_input == 'c'
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif !Common.numeric?(user_input)
        puts 'ID must be integer!'
        puts
        next
      else
        customer_id = user_input
        id_exists = id_exists?(customer_id)
        if id_exists
          @@customer_controller.delete_customer_projects(customer_id)
          @@customer_controller.delete(customer_id)
          id_exists = id_exists?(customer_id)
          puts
          puts "Customer with id = #{customer_id} has successfully deleted" unless id_exists
          puts
        else
          puts "No customer with id = #{customer_id}"
          puts
          next
        end
      end
    end
    @exit = true
  end

  def create_customer
    @exit = false
    @item = 'customer'

    puts 'Searching existing customers:'
    Decor.run
    puts
    show_all_customers
    puts

    until @exit
      @@id = @@item_view.item_view(@item, 'id')
      if @@id == true
        @exit = true
        break
      end
      id_exists = id_exists?(@@id)
      if id_exists
        puts "ID #{@@id} is already exists. Please, choose another ID."
        puts
        next
      else
        break
      end
    end

    until @exit
      @@name = @@item_view.item_view(@item, 'name')
      if @@name == true
        @exit = true
        break
      end
      name_exists = name_exists?(@@name)
      if name_exists
        puts "Name '#{@@name}' is already exists. Please, choose another name."
        puts
        next
      else
        break
      end
    end

    until @exit
      customer = Customer.new
      customer.id = @@id
      customer.name = @@name

      @@customer_controller.save(customer)

      id_exists = id_exists?(@@id)
      puts
      puts "Customer #{@@name} with id = #{@@id} has saved" if id_exists
      puts
      break
    end

    until @exit
      puts 'Add project to customer? Yes(y) or No(n):'
      user_input = gets.chomp.strip

      if user_input.include?('n') || user_input.include?('no')
        break
      elsif user_input == '' || !user_input.include?('n') || !user_input.include?('no') || !user_input.include?('y') || !user_input.include?('yes')
        next
      elsif user_input.include?('y') || user_input.include?('yes')
        add_project(@@id)
        break
      end
    end

    until @exit
      puts 'Create another one customer? Yes(y) or No(n):'
      user_input = gets.chomp.strip

      if user_input.include?('n') || user_input.include?('no')
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif user_input == ''
        next
      elsif user_input.include?('y') || user_input.include?('yes')
        break
      end
    end
    @@id = nil
    @@name = nil
  end

  def update_customer
    @exit = false
    @item = 'customer'
    @customer_id = nil

    puts 'Searching existing customers:'
    Decor.run
    puts
    show_all_customers
    puts
    puts "Enter customer id you're going to update or c to cancel:"
    @user_input = gets.chomp.strip

    until @exit
      if @user_input == 'c'
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif !Common.numeric?(@user_input)
        puts 'ID must be integer!'
        puts
        next
      else
        @customer_id = @user_input
        @old_customer = @@customer_controller.get_by_id(@customer_id)
        if @old_customer.nil?
          puts "No customer with id = #{@user_input}"
          next
        else
          puts '==========='
          puts 'ID: ' + @old_customer[0]['id']
          puts 'Name: ' + @old_customer[0]['name']
          puts
          break
        end
      end
    end

    until @exit
      puts 'Change: '
      puts '    1.name?'
      puts '    2.project?'
      puts '8.Cancel'

      @user_input = gets.chomp.strip

      if @user_input == '8'
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif @user_input != '8' && @user_input != '2' && @user_input != '1'
        puts 'Enter 1-2 or 8 to cancel.'
        puts
        next
      else
        case @user_input
        when '1'
          @user_input = @@item_view.item_view(@item, 'name')

          new_customer = Customer.new
          new_customer.id = @old_customer[0]['id']
          new_customer.name = @user_input

          @@customer_controller.update(new_customer)
          @old_customer.clear

          puts 'Change project? y = yes, n = enter whatever:'
          change = gets.chomp

          if change.include?('y') || change.include?('yes')
            change_project(@customer_id)
            Decor.exiting
            ConsoleHelper.console_helper
            @exit =  true
          else
            @exit = true
            Decor.exiting
            ConsoleHelper.console_helper
          end
        when '2'
          change_project(@customer_id)
          @exit = true
          Decor.exiting
          ConsoleHelper.console_helper
        else
          puts 'Else in case block'
        end
      end
    end
  end

  def change_project(customer_id)
    @exit = false

    until @exit
      puts 'You want to:'
      puts '      1.add new project to customer'
      puts '      2.delete project from customer'

      user_input = gets.strip

      case user_input
      when '1'
        until @exit
          add_new_project_to_customer(customer_id)
          @exit = true
          break
        end
      when '2'
        until @exit
          delete_project_from_customer(customer_id)
          @exit = true
          break
        end
      else
        puts 'Else in case block'
      end
    end
  end

  def add_absent_project(customer_id, project_id)
    @exit = false

    until @exit
      project = @@project_controller.get_by_id(project_id)
      if project.nil?
        puts "No project with id = #{project_id}"
        next
      else
        project_id = project[0]['id']
        @@customer_controller.customers_projects(customer_id, project_id)
        @exit = true
        break
      end
    end
  end

  def add_project(customer_id)
    @exit = false

    @@project_controller.show_all

    until @exit
      puts 'Choose id of project or c to cancel:'
      user_input = gets.strip
      project = @@project_controller.get_by_id(user_input)
      if project.nil?
        puts "No project with id = #{user_input}"
        puts
      elsif user_input == 'c'
        break
      elsif !Common.numeric?(user_input)
        puts 'ID must be numeric!'
        puts
        next
      else
        project_id = project[0]['id']
        @@customer_controller.customers_projects(customer_id, project_id)
      end

      puts 'Add another one project? y = yes, n = no:'
      user_input = gets.chomp
      if user_input.include?('y') || user_input.include?('yes')
        next
      else
        break
      end
    end
  end

  def add_new_project_to_customer(customer_id)
    @exit = false

    until @exit
      puts 'Choose id of project or c to cancel:'
      puts
      customer_projects = []
      projects_id = @@customer_controller.show_all_projects_by_cus_id(customer_id)

      if projects_id.nil?
        projects = @@project_controller.show_all
        if projects.nil?
          puts 'No existing projects. Create projects first.'
          puts
          break
        else
          projects.each do |project|
            puts '=========='
            puts 'ID: %s' % [project['id']]
            puts 'Name: %s' % [project['name']]
            puts 'Version: %s' % [project['version']]
            puts 'Cost: %s' % [project['cost']]
            puts
          end
        end
        user_input = gets.strip
        if user_input == 'c'
          @exit = true
          break
        elsif !Common.numeric?(user_input)
          puts 'ID must be an integer!'
          puts
          next
        else
          add_absent_project(customer_id, user_input)
          puts 'Project added.'
          next
        end
      else
        projects_id.each do |id|
          customer_projects << id['project_id']
        end

        absent_projects = @@customer_controller.show_absent_projects(customer_projects)

        absent_projects.each do |row|
          puts '================='
          puts 'ID: ' + row['id']
          puts 'Name: ' + row['name']
          puts 'Version: ' + row['version']
          puts 'Cost: ' + row['cost']
        end

        user_input = gets.strip
        if user_input == 'c'
          @exit = true
          break
        elsif !Common.numeric?(user_input)
          puts 'ID must be integer!'
          puts
          next
        else
          if project_exists?(customer_id, user_input)
            puts "Customer already have the project with id = #{user_input}"
            puts
            next
          else
            add_absent_project(customer_id, user_input)
            break
          end
        end
      end
    end
  end

  def delete_project_from_customer(customer_id)
    @exit = false

    until @exit
      ids_of_customer_projects = []
      projects_id = @@customer_controller.show_all_projects_by_cus_id(customer_id)
      if projects_id.nil?
        puts 'Customer have no projects. Nothing to delete...'
        puts
        @exit = true
        break
      else
        puts 'Projects of customer:'
        projects_id.each do |id|
          project = @@project_controller.get_by_id(id['project_id'])
          ids_of_customer_projects << project[0]['id']
          puts '============='
          puts 'ID: ' + project[0]['id']
          puts 'Name: ' + project[0]['name']
          puts 'Version: ' + project[0]['version']
          puts 'Cost: ' + project[0]['cost']
          puts
        end
      end
      puts 'Choose id of project to delete or c to cancel:'

      user_input = gets.strip
      if user_input == 'c'
        break
      elsif !Common.numeric?(user_input)
        puts 'ID must be integer!'
        next
      else
        result = @@project_controller.get_by_id(user_input)
        if !ids_of_customer_projects.include?(user_input)
          puts "Customer have no project with id = #{user_input}"
          puts
        else
          project_id = result[0]['id']
          @@customer_controller.delete_projects_customers(project_id)
          puts 'Project deleted'
          @exit = true
          break
        end
      end
    end
  end

  def id_exists?(customer_id)
    id = @@customer_controller.get_by_id(customer_id)

    return false if id.nil?
    true unless id.nil?
  end

  def name_exists?(customer_name)
    customer = @@customer_controller.get_by_name(customer_name)

    return false if customer.nil?
    true unless customer.nil?
  end

  def project_exists?(customer_id, project_id)
    projects_ids = @@customer_controller.show_all_projects_by_cus_id(customer_id)

    projects_ids.each do |row|
      if project_id == row['id']
        return true
      else
        return false
      end
    end
  end
end