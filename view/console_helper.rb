require_relative 'developer_view'
require_relative 'project_view'
require_relative 'skill_view'
require_relative 'company_view'
require_relative 'customer_view'
require './common/decorations/decor'

class ConsoleHelper
  def self.console_helper
    developer_view = DeveloperView.new
    project_view = ProjectView.new
    skill_view = SkillView.new
    company_view = CompanyView.new
    customer_view = CustomerView.new

    exit = false

    until exit
      begin
        puts
        puts 'Choose, what do you want to do: '
        puts '    Create: '
        puts '            1. Skill'
        puts '            2. Project'
        puts '            3. Developer'
        puts '            4. Company'
        puts '            5. Customer'
        puts '-----------------------'
        puts '    Update: '
        puts '            6. Skill'
        puts '            7. Project'
        puts '            8. Developer'
        puts '            9. Company'
        puts '            10. Customer'
        puts '-----------------------'
        puts '    Find by ID: '
        puts '            11. Skill'
        puts '            12. Project'
        puts '            13. Developer'
        puts '            14. Company'
        puts '            15. Customer'
        puts '-----------------------'
        puts '    Show all: '
        puts '            16. Skills'
        puts '            17. Projects'
        puts '            18. Developers'
        puts '            19. Companies'
        puts '            20. Customers'
        puts '-----------------------'
        puts '    Delete: '
        puts '            21. Skill'
        puts '            22. Project'
        puts '            23. Developer'
        puts '            24. Company'
        puts '            25. Customer'
        puts '-----------------------'
        puts '    Exit: '
        puts '            26. Exit'
        puts

        user_input = gets.chomp.strip

        case user_input
        when '1'
          skill_view.create
          break
        when '2'
          project_view.create
          break
        when '3'
          developer_view.create
          break
        when '4'
          company_view.create
          break
        when '5'
          customer_view.create
          break
        when '6'
          skill_view.update
          break
        when '7'
          project_view.update
          break
        when '8'
          developer_view.update
          break
        when '9'
          company_view.update
          break
        when '10'
          customer_view.update
          break
        when '11'
          skill_view.get_by_id
          break
        when '12'
          project_view.get_by_id
          break
        when '13'
          developer_view.get_by_id
          break
        when '14'
          company_view.get_by_id
          break
        when '15'
          customer_view.get_by_id
          break
        when '16'
          skill_view.show_all
          break
        when '17'
          project_view.show_all
          break
        when '18'
          developer_view.show_all
          break
        when '19'
          company_view.show_all
          break
        when '20'
          customer_view.show_all
          break
        when '21'
          skill_view.delete
          break
        when '22'
          project_view.delete
          break
        when '23'
          developer_view.delete
          break
        when '24'
          company_view.delete
          break
        when '25'
          customer_view.delete
          break
        when '26'
          exit = true
          puts 'Exiting'
          Decor.run
          break if exit
        else
          puts 'Enter 1-25 or 26 to exit'
          sleep 2
        end
      end
    end
  end
end