require './controller/company_controller'
require './controller/project_controller'
require './model/company'
require './common/decorations/decor'
require './common/common'
require_relative 'console_helper'
require_relative 'item_view'

class CompanyView
  @@company_controller = CompanyController.new
  @@project_controller = ProjectController.new
  @@item_view = ItemView.new

  @@id = nil
  @@name = nil

  def create
    exit = false
    create_company until exit
    exit = true
  end

  def update
    @exit = false
    update_company until @exit
    @exit = true
  end

  def get_by_id
    @exit = false

    until @exit
      puts 'Enter company id or c to cancel:'
      user_input = gets.chomp.strip

      if user_input == 'c'
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif !Common.numeric?(user_input)
        puts 'ID must be integer!'
        puts
        next
      else
        id_exists = id_exists?(user_input)
        if id_exists
          company = @@company_controller.get_by_id(user_input)
          company.each do |row|
            puts '===================='
            puts 'ID: %s' % [row['id']]
            puts 'Name: %s' % [row['name']]
            puts '---'
            puts 'Projects: '
            projects_ids = @@company_controller.show_all_projects_by_com_id(row['id'])
            if projects_ids.nil?
              puts '         -- No projects --'
              puts
            else
              projects_ids.each do |project|
                project_id = project['project_id']
                project = @@project_controller.get_by_id(project_id)
                puts '         ID: %s' % [project[0]['id']]
                puts '         Name: %s' % [project[0]['name']]
                puts '         Cost: %s' % [project[0]['cost']]
                puts '         Version: %s' % [project[0]['version']]
                puts '         -'
                puts
              end
              projects_ids.clear
            end
          end
          company.clear
        else
          puts "There is no company with id = #{user_input}"
          puts
        end
      end
    end
  end

  def show_all
    @exit = false

    until @exit
      show_all_companies
      puts
      puts 'Back to main menu - enter m'
      user_input = gets.strip
      @exit = true if user_input.include?('m') || user_input.include?('') || user_input.include?('\n')
      ConsoleHelper.console_helper
    end
    @exit = true
  end

  def show_all_companies
    companies = @@company_controller.show_all
    if companies.nil?
      puts '----- ' + 'No companies yet' + ' -----'
    else
      companies.each do |company|
        puts '===================='
        puts 'ID: %s' % [company['id']]
        puts 'Name: %s' % [company['name']]
        puts '---'
        puts 'Projects: '
        projects_ids = @@company_controller.show_all_projects_by_com_id(company['id'])
        if projects_ids.nil?
          puts '         -- No projects --'
          puts
        else
          projects_ids.each do |project|
            project_id = project['project_id']
            project = @@project_controller.get_by_id(project_id)
            puts '         ID: %s' % [project[0]['id']]
            puts '         Name: %s' % [project[0]['name']]
            puts '         Cost: %s' % [project[0]['cost']]
            puts '         Version: %s' % [project[0]['version']]
            puts '         -'
          end
          projects_ids.clear
        end
      end
      companies.clear
    end
  end

  def delete
    @exit = false

    puts 'Searching existing companies:'
    Decor.run
    puts
    show_all_companies
    puts

    until @exit
      puts 'Enter company id or c to cancel:'
      user_input = gets.chomp.strip

      if user_input == 'c'
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif !Common.numeric?(user_input)
        puts 'ID must be integer!'
        puts
        next
      else
        company_id = user_input
        id_exists = id_exists?(company_id)
        if id_exists
          @@company_controller.delete_company_projects(company_id)
          @@company_controller.delete(company_id)
          id_exists = id_exists?(company_id)
          puts
          puts "Company with id = #{company_id} has successfully deleted" unless id_exists
          puts
        else
          puts "No company with id = #{company_id}"
          puts
          next
        end
      end
    end
    @exit = true
  end

  def create_company
    exit = false
    item = 'company'

    puts 'Searching existing companies:'
    Decor.run
    puts
    show_all_companies
    puts

    until exit
      @@id = @@item_view.item_view(item, 'id')
      if @@id == true
        exit = true
        break if exit
      end
      id_exists = id_exists?(@@id)
      if id_exists
        puts "ID #{@@id} is already exists. Please, choose another ID."
        puts
        next
      else
        break
      end
    end

    until exit
      @@name = @@item_view.item_view(item, 'name')
      if @@name == true
        exit = true
        break if exit
      end
      name_exists = name_exists?(@@name)
      if name_exists
        puts "Name '#{@@name}' is already exists. Please, choose another name."
        puts
        next
      else
        break
      end
    end

    until exit
      company = Company.new
      company.id = @@id
      company.name = @@name

      @@company_controller.save(company)

      id_exists = id_exists?(@@id)
      puts
      puts "Company '#{@@name}' with id = #{@@id} has saved" if id_exists
      puts
      break
    end

    until exit
      puts 'Add project to company? Yes(y) or No(n):'
      user_input = gets.chomp.strip

      if user_input.include?('n') || user_input.include?('no')
        break
      elsif user_input == '' || !user_input.include?('n') || !user_input.include?('no') || !user_input.include?('y') || !user_input.include?('yes')
        next
      elsif user_input.include?('y') || user_input.include?('yes')
        puts 'Company ID: ' + @@id
        add_project(@@id)
        break
      end
    end

    until exit
      puts 'Create another one company? Yes(y) or No(n):'
      user_input = gets.chomp.strip

      if user_input.include?('n') || user_input.include?('no')
        exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif user_input == '' || !user_input.include?('n') || !user_input.include?('no') || !user_input.include?('y') || !user_input.include?('yes')
        next
      elsif user_input.include?('y') || user_input.include?('yes')
        break
      end
    end
    @@id = nil
    @@name = nil
  end

  def update_company
    @exit = false
    @item = 'company'
    @company_id = nil


    puts 'Searching existing companies:'
    Decor.run
    puts
    show_all_companies
    puts
    puts "Enter company id you're going to update or c to cancel:"
    @user_input = gets.chomp.strip

    until @exit
      if @user_input == 'c'
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif !Common.numeric?(@user_input)
        puts 'ID must be integer!'
        puts
        next
      else
        @old_company = @@company_controller.get_by_id(@user_input)
        @company_id = @user_input
        if @old_company.nil?
          puts "No company with id = #{@user_input}"
        else
          puts '==========='
          puts 'ID: ' + @old_company[0]['id']
          puts 'Name: ' + @old_company[0]['name']
          puts
          break
        end
      end
    end

    until @exit
      puts 'Change: '
      puts '    1.name?'
      puts '    2.project?'
      puts '8.Cancel'

      @user_input = gets.chomp.strip

      if @user_input == '8'
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif @user_input != '8' && @user_input != '2' && @user_input != '1'
        puts 'Enter 1-2 or 8 to cancel.'
        puts
      else
        case @user_input
        when '1'
        @user_input = @@item_view.item_view(@item, 'name')

        new_company = Company.new
        new_company.id = @old_company[0]['id']
        new_company.name = @user_input

        @@company_controller.update(new_company)
        @old_company.clear

        puts 'Change project? y = yes, n = enter whatever:'
        change = gets.chomp

        if change.include?('y') || change.include?('yes')
          change_project(@company_id)
          Decor.exiting
          ConsoleHelper.console_helper
          @exit =  true
        else
          @exit = true
          Decor.exiting
          ConsoleHelper.console_helper
        end
        when '2'
          change_project(@company_id)
          @exit = true
          Decor.exiting
          ConsoleHelper.console_helper
        else
          puts 'Else in case block...'
        end
      end
    end
  end

  def change_project(company_id)
    @exit = false

    until @exit
      puts 'You want to:'
      puts '      1.add new project to company'
      puts '      2.delete project from company'

      user_input = gets.strip

      case user_input
      when '1'
        until @exit
          add_new_project_to_company(company_id)
          @exit = true
          break
        end
      when '2'
        until @exit
          delete_project_from_company(company_id)
          @exit = true
          break
        end
      else
        puts 'Else in case block'
      end
    end
  end

  def add_absent_project(company_id, project_id)
    @exit = false

    until @exit
      project = @@project_controller.get_by_id(project_id)
      if project.nil?
        puts "No project with id = #{project_id}"
        next
      else
        project_id = project[0]['id']
        @@company_controller.companies_projects(company_id, project_id)
        @exit = true
        break
      end
    end
  end

  def add_project(company_id)
    @exit = false

    @@project_controller.show_all

    until @exit
      puts 'Choose id of project or c to cancel:'
      user_input = gets.strip
      project = @@project_controller.get_by_id(user_input)
      if project.nil?
        puts "No project with id = #{user_input}"
        puts
      elsif user_input == 'c'
        break
      elsif Common.numeric?(user_input)
        puts 'ID must be numeric!'
        puts
        next
      else
        project_id = project[0]['id']
        @@company_controller.companies_projects(company_id, project_id)
      end

      puts 'Add another one project? y = yes, n = no:'
      user_input = gets.chomp
      if user_input.include?('y') || user_input.include?('yes')
        next
      else
        break
      end
    end
  end

  def add_new_project_to_company(company_id)
    @exit = false

    until @exit
      puts 'Choose id of project or c to cancel:'
      puts
      company_projects = []
      projects_id = @@company_controller.show_all_projects_by_com_id(company_id)

      if projects_id.nil?
        projects = @@project_controller.show_all
        if projects.nil?
          puts 'No existing projects. Creat projects first.'
          puts
          break
        else
          projects.each do |project|
            puts '=========='
            puts 'ID: %s' % [project['id']]
            puts 'Name: %s' % [project['name']]
            puts 'Version: %s' % [project['version']]
            puts 'Cost: %s' % [project['cost']]
            puts
          end
        end
        user_input = gets.strip
        if user_input == 'c'
          break
        elsif !Common.numeric?(user_input)
          puts 'ID must be integer!'
          puts
          next
        else
          add_absent_project(company_id, user_input)
        end
      else
        projects_id.each do |id|
          company_projects << id['project_id']
        end

        absent_projects = @@customer_controller.show_absent_projects(company_projects)

        absent_projects.each do |row|
          puts '================='
          puts 'ID: ' + row['id']
          puts 'Name: ' + row['name']
          puts 'Version: ' + row['version']
          puts 'Cost: ' + row['cost']
        end

        user_input = gets.strip
        if user_input == 'c'
          break
        elsif !Common.numeric?(user_input)
          puts 'ID must be integer!'
          puts
          next
        else
          if project_exists?(company_id, user_input)
            puts "Company already have the project with id = #{user_input}"
            puts
            next
          else
            add_absent_project(company_id, user_input)
            break
          end
        end
      end
    end
  end

  def delete_project_from_company(company_id)
    @exit = false

    until @exit
      ids_of_company_projects = []
      projects_id = @@customer_controller.show_all_projects_by_com_id(company_id)
      if projects_id.nil?
        puts 'Company have no projects. Nothing to delete...'
        @exit = true
        break
      else
        puts 'Projects of company:'
        projects_id.each do |id|
          project = @@project_controller.get_by_id(id['project_id'])
          ids_of_company_projects << project[0]['id']
          puts '============='
          puts 'ID: ' + project[0]['id']
          puts 'Name: ' + project[0]['name']
          puts 'Version: ' + project[0]['version']
          puts 'Cost: ' + project[0]['cost']
          puts
        end
      end
      puts 'Choose id of project to delete or c to cancel:'

      user_input = gets.strip
      if user_input == 'c'
        break
      elsif !Common.numeric?(user_input)
        puts 'ID must be integer!'
        next
      else
        result = @@project_controller.get_by_id(user_input)
        if !ids_of_company_projects.include?(user_input)
          puts "Company have no project with id = #{user_input}"
          puts
        else
          project_id = result[0]['id']
          @@customer_controller.delete_projects_companies(project_id)
          puts 'Project deleted'
          @exit = true
          break
        end
      end
    end
  end

  def id_exists?(company_id)
    id = @@company_controller.get_by_id(company_id)

    return false if id.nil?
    true unless id.nil?
  end

  def name_exists?(company_name)
    company = @@company_controller.get_by_name(company_name)

    return false if company.nil?
    true unless company.nil?
  end

  def project_exists?(company_id, project_id)
    projects_ids = @@company_controller.show_all_projects_by_com_id(company_id)

    projects_ids.each do |row|
      if project_id == row['id']
        return true
      else
        return false
      end
    end
  end
end