require './common/decorations/decor'

class ItemView
  def item_view(item, attr)
    @exit = false

    until @exit
      puts "Enter new #{item} #{attr} or c to cancel:"
      user_input = gets.chomp.strip

      if user_input == 'c'
        Decor.exiting
        ConsoleHelper.console_helper
        return true
      elsif user_input == ''
        next
      else
        @exit = true
        return user_input
      end
    end
    @exit = true
  end
end