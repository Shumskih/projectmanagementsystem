require './controller/skill_controller'
require './model/skill'
require './common/decorations/decor'
require_relative 'console_helper'
require_relative 'item_view'

class SkillView
  @@skill_controller = SkillController.new
  @@item_view = ItemView.new

  @@id = nil
  @@name = nil

  def create
    @exit = false
    create_skill until @exit
    @exit = true
  end

  def update
    exit = false
    until exit
      update_skill
      exit = true
    end
  end

  def get_by_id
    exit = false

    until exit
      puts 'Enter skill id or c to cancel:'
      user_input = gets.chomp.strip

      if user_input == 'c'
        exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif user_input == ''
        next
      else
        id_exists = id_exists?(user_input)
        if id_exists
          skill = @@skill_controller.get_by_id(user_input)
          skill.each do |row|
            puts '===================='
            puts 'ID: %s' % [row['id']]
            puts 'Name: %s' % [row['name']]
            puts
          end
          skill.clear
        else
          puts "There is no skill with id = #{user_input}"
          puts
        end
      end
    end
  end

  def show_all
    exit = false

    until exit
      show_all_skills
      puts
      puts 'Back to main menu - enter m'
      user_input = gets.strip
      if user_input.include?('m') || user_input.include?('') || user_input.include?('\n')
        exit = true
        ConsoleHelper.console_helper
      end
    end
  end

  def show_all_skills
    skills = @@skill_controller.show_all
    if skills.nil?
      puts
      puts '----- ' + 'No skills yet' + ' -----'
      puts
    else
      skills.each do |skill|
        puts '===================='
        puts 'ID: %s' % [skill['id']]
        puts 'Name: %s' % [skill['name']]
        puts
      end
      skills.clear
    end
  end

  def delete
    exit = false

    puts 'Searching existing skills:'
    Decor.run
    puts
    show_all_skills
    puts

    until exit
      puts 'Enter skill id to delete or c to cancel:'
      user_input = gets.chomp.strip

      if user_input == 'c'
        exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif !Common.numeric?(user_input)
        puts 'ID must be integer!'
        puts
        next
      else
        skill_id = user_input
        id_exists = id_exists?(skill_id)
        if id_exists
          @@skill_controller.delete_skill_developer(skill_id)
          @@skill_controller.delete(skill_id)
          id_exists = id_exists?(skill_id)
          puts
          puts "Skill with id = #{skill_id} has successfully deleted" unless id_exists
          puts
        else
          puts "No skill with id = #{skill_id}"
          puts
          next
        end
      end
    end
  end

  def create_skill
    @exit = false
    item = 'skill'

    puts 'Searching existing skills:'
    Decor.run
    puts
    show_all_skills
    puts

    until @exit
      @@id = @@item_view.item_view(item, 'id')
      if @@id == true
        @exit = true
        break
      end
      id_exists = id_exists?(@@id)
      if id_exists
        puts "ID #{@@id} is already exists. Please, choose another ID."
        puts
        next
      else
        break
      end
    end

    until @exit
      @@name = @@item_view.item_view(item, 'name')
      if @@name == true
        @exit = true
        break
      end
      name_exists = name_exists?(@@name)
      if name_exists
        puts "Name '#{@@name}' is already exists. Please, choose another name."
        puts
        next
      else
        break
      end
    end

    until @exit
      skill = Skill.new
      skill.id = @@id
      skill.name = @@name

      @@skill_controller.save(skill)

      id_exists = id_exists?(@@id)
      puts
      puts "Skill #{@@name} with id = #{@@id} has saved" if id_exists
      puts
      break
    end

    until @exit
      puts 'Create another one skill? Yes(y) or No(n):'
      user_input = gets.chomp.strip

      if user_input.include?('n') || user_input.include?('no')
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif user_input == ''
        next
      elsif user_input.include?('y') || user_input.include?('yes')
        break
      end
    end
    @@id = nil
    @@name = nil
  end

  def update_skill
    @exit = false
    @item = 'skill'
    @skill_id = nil

    puts 'There are all existing skills:'
    puts
    @@skill_controller.show_all
    puts
    puts "Enter skill id you're going to update or c to cancel:"
    @user_input = gets.chomp.strip

    until @exit
      if @user_input == 'c'
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif @user_input == ''
        next
      else
        @old_skill = @@skill_controller.get_by_id(@user_input)
        if @old_skill.nil?
          puts "No skill with id = #{@user_input}"
        else
          puts '==========='
          puts 'ID: ' + @old_skill[0]['id']
          puts 'Name: ' + @old_skill[0]['name']
          puts
          break
        end
      end
    end

    until @exit
      puts 'Change: '
      puts '    1.name?'
      puts '8.Cancel'

      @user_input = gets.chomp.strip

      if @user_input == '8'
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif @user_input != '8' && @user_input != '1'
        puts 'Enter 1 to change name or 8 to cancel.'
        puts
      elsif @user_input == '1'
        @user_input = @@item_view.item_view(@item, 'name')

        new_skill = Skill.new
        new_skill.id = @old_skill[0]['id']
        new_skill.name = @user_input

        @@skill_controller.update(new_skill)
        @old_skill.clear
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      end
    end
  end

  def id_exists?(skill_id)
    id = @@skill_controller.get_by_id(skill_id)

    return false if id.nil?
    true unless id.nil?
  end

  def name_exists?(skill_name)
    skill = @@skill_controller.get_by_name(skill_name)

    return false if skill.nil?
    true unless skill.nil?
  end
end