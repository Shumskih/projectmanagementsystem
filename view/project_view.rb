require './controller/project_controller'
require './model/project'
require './common/decorations/decor'
require './common/common'
require_relative 'console_helper'
require_relative 'item_view'

class ProjectView
  @@project_controller = ProjectController.new
  @@item_view = ItemView.new

  @@id = nil
  @@name = nil
  @@version = nil
  @@cost = nil

  def create
    @exit = false
    create_project until @exit
    @exit = true
  end

  def update
    @exit = false
    update_project until @exit
    @exit = true
  end

  def get_by_id
    exit = false

    until exit
      puts 'Enter project id or c to cancel:'
      user_input = gets.chomp.strip

      if user_input == 'c'
        exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif !Common.numeric?(user_input)
        puts 'ID must be an integer!'
        puts
        next
      else
        id_exists = id_exists?(user_input)
        if id_exists
          project = @@project_controller.get_by_id(user_input)
          project.each do |row|
            puts '===================='
            puts 'ID: %s' % [row['id']]
            puts 'Name: %s' % [row['name']]
            puts 'Version: %s' % [row['version']]
            puts 'Cost: %s' % [row['cost']]
            puts
          end
          project.clear
        else
          puts "There is no project with id = #{user_input}"
          puts
        end
      end
    end
  end

  def show_all
    @exit = false

    until @exit
      show_all_projects
      puts
      puts 'Back to main menu - enter m'
      user_input = gets.strip
      @exit = true if user_input.include?('m') || user_input.include?('') || user_input.include?('\n')
      ConsoleHelper.console_helper
    end
  end

  def show_all_projects
    projects = @@project_controller.show_all
    if projects.nil?
      puts '----- ' + 'No projects yet' + ' -----'
    else
      projects.each do |project|
        puts '===================='
        puts 'ID: %s' % [project['id']]
        puts 'Name: %s' % [project['name']]
        puts 'Version: %s' % [project['version']]
        puts 'Cost: %s' % [project['cost']]
        puts
      end
      projects.clear
    end
  end

  def delete
    @exit = false

    puts 'Searching existing projects:'
    Decor.run
    puts
    show_all_projects
    puts

    until @exit
      puts 'Enter project id or c to cancel:'
      user_input = gets.chomp.strip

      if user_input == 'c'
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif !Common.numeric?(user_input)
        puts 'ID must be integer!'
        puts
        next
      else
        project_id = user_input
        id_exists = id_exists?(project_id)
        if id_exists
          @@project_controller.delete_companies_projects(user_input)
          @@project_controller.delete_customers_projects(user_input)
          @@project_controller.delete_developers_projects(user_input)
          @@project_controller.delete(user_input)
          id_exists = id_exists?(project_id)
          puts
          puts "Project with id = #{project_id} has successfully deleted" unless id_exists
          puts
        else
          puts "No project with id = #{project_id}"
          puts
          next
        end
      end
    end
    @exit = true
  end

  def create_project
    @exit = false
    @item = 'project'

    puts 'Searching existing projects:'
    Decor.run
    puts
    show_all_projects
    puts

    until @exit
      @@id = @@item_view.item_view(@item, 'id')
      if @@id == true
        @exit = true
        break
      end
      id_exists = id_exists?(@@id)
      if id_exists
        puts "ID #{@@id} is already exists. Please, choose another ID."
        puts
        next
      else
        break
      end
    end

    until @exit
      @@name = @@item_view.item_view(@item, 'name')
      if @@name == true
        @exit = true
        break
      end
      name_exists = name_exists?(@@name)
      if name_exists
        puts "Name '#{@@name}' is already exists. Please, choose another name."
        puts
        next
      else
        break
      end
    end

    until @exit
      @@version = @@item_view.item_view(@item, 'version')
      @exit = true if @@version == true
      break
    end

    until @exit
      @@cost = @@item_view.item_view(@item, 'cost')
      @exit = true if @@cost == true
      break
    end

    until @exit
      project = Project.new
      project.id = @@id
      project.name = @@name
      project.version = @@version
      project.cost = @@cost

      @@project_controller.save(project)

      id_exists = id_exists?(@@id)
      puts
      puts "Project '#{@@name}' with id = #{@@id} has saved" if id_exists
      puts
      break
    end

    until @exit
      puts 'Create another one project? Yes(y) or No(n):'
      user_input = gets.chomp.strip

      if user_input.include?('n') || user_input.include?('no')
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif user_input == ''
        next
      elsif user_input.include?('y') || user_input.include?('yes')
        break
      end
    end
    @@id = nil
    @@name = nil
    @@version = nil
    @@cost = nil
  end

  def update_project
    @exit = false
    @item = 'project'
    @project_id = nil

    puts 'Searching existing projects:'
    Decor.run
    puts
    show_all_projects
    puts
    puts "Enter project id you're going to update or c to cancel:"
    @user_input = gets.chomp.strip

    until @exit
      if @user_input == 'c'
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif !Common.numeric?(@user_input)
        puts 'ID must be integer!'
        puts
        next
      else
        @old_project = @@project_controller.get_by_id(@user_input)
        if @old_project.nil?
          puts "No project with id = #{@user_input}"
          next
        else
          puts '=========='
          puts 'ID: ' + @old_project[0]['id']
          puts 'Name: ' + @old_project[0]['name']
          puts 'Version: ' + @old_project[0]['version']
          puts 'Cost: ' + @old_project[0]['cost']
          break
        end
      end
    end

    until @exit
      puts 'Change: '
      puts '    1.name?'
      puts '    2.version?'
      puts '    3.cost?'
      puts '8.Cancel'

      @user_input = gets.chomp.strip

      if @user_input == '8'
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      elsif @user_input != '8' && @user_input != '3' && @user_input != '2' && @user_input != '1'
        puts 'Enter 1-2 or 8 to cancel.'
        puts
        next
      else
        case @user_input
        when '1'
          @user_input = @@item_view.item_view(@item, 'name')

          new_project = Project.new
          new_project.id = @old_project[0]['id']
          new_project.name = @user_input
          new_project.version = @old_project[0]['version']
          new_project.cost = @old_project[0]['cost']

          @@project_controller.update(new_project)
        when '2'
          @user_input = @@item_view.item_view(@item, 'version')

          new_project = Project.new
          new_project.id = @old_project[0]['id']
          new_project.name = @old_project[0]['name']
          new_project.version = @user_input
          new_project.cost = @old_project[0]['cost']

          @@project_controller.update(new_project)
        when '3'
          @user_input = @@item_view.item_view(@item, 'cost')

          new_project = Project.new
          new_project.id = @old_project[0]['id']
          new_project.name = @old_project[0]['name']
          new_project.version = @old_project[0]['version']
          new_project.cost = @user_input

          @@project_controller.update(new_project)
        else
          puts 'Oooops'
        end
        @old_project.clear
        @exit = true
        Decor.exiting
        ConsoleHelper.console_helper
      end
    end
  end

  def id_exists?(project_id)
    id = @@project_controller.get_by_id(project_id)

    return false if id.nil?
    true unless id.nil?
  end

  def name_exists?(project_name)
    project = @@project_controller.get_by_name(project_name)

    return false if project.nil?
    true unless project.nil?
  end
end