class Decor
  def self.run
    i = 0
    dot = '.'

    while i < 10
      print dot
      i += 1
      sleep 0.2
    end
  end

  def self.exiting
    puts 'Exiting'
    Decor.run
  end
end
