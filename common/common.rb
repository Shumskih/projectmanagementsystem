class Common
  def self.numeric?(integer)
    integer =~ /[0-9]/
  end
end