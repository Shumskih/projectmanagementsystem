require './dao/project_dao'

class ProjectController
  @@project_dao = ProjectDao.new

  def save(project)
    @@project_dao.save(project)
  end

  def get_by_id(project_id)
    @@project_dao.get_by_id(project_id)
  end

  def get_by_name(project_name)
    @@project_dao.get_by_name(project_name)
  end

  def show_all
    @@project_dao.show_all
  end

  def update(project)
    @@project_dao.update(project)
  end

  def delete(project_id)
    @@project_dao.delete(project_id)
  end

  def delete_companies_projects(project_id)
    @@project_dao.delete_companies_projects(project_id)
  end

  def delete_customers_projects(project_id)
    @@project_dao.delete_customers_projects(project_id)
  end

  def delete_developers_projects(project_id)
    @@project_dao.delete_developers_projects(project_id)
  end
end