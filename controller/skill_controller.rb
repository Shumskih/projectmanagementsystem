require './dao/skill_dao'

class SkillController
  @@skill_dao = SkillDao.new

  def save(skill)
    @@skill_dao.save(skill)
  end

  def get_by_id(skill_id)
    @@skill_dao.get_by_id(skill_id)
  end

  def get_by_name(skill_name)
    @@skill_dao.get_by_name(skill_name)
  end

  def show_all
    @@skill_dao.show_all
  end

  def update(skill)
    @@skill_dao.update(skill)
  end

  def delete(skill_id)
    @@skill_dao.delete(skill_id)
  end

  def show_all_ids
    @@skill_dao.show_all_ids
  end

  def delete_skill_developer(skill_id)
    @@skill_dao.delete_skill_developer(skill_id)
  end
end