require './dao/company_dao'

class CompanyController
  @@company_dao = CompanyDao.new

  def save(company)
    @@company_dao.save(company)
  end

  def get_by_id(company_id)
    @@company_dao.get_by_id(company_id)
  end

  def get_by_name(company_name)
    @@company_dao.get_by_name(company_name)
  end

  def show_all
    @@company_dao.show_all
  end

  def update(company)
    @@company_dao.update(company)
  end

  def delete(company_id)
    @@company_dao.delete(company_id)
  end

  def delete_company_projects(company_id)
    @@company_dao.delete_company_projects(company_id)
  end

  def companies_projects(company_id, project_id)
    @@company_dao.companies_projects(company_id, project_id)
  end

  def show_all_projects_by_com_id(company_id)
    @@company_dao.show_all_projects_by_com_id(company_id)
  end

  def show_absent_projects(existing_project_ids)
    @@company_dao.show_absent_projects(existing_project_ids)
  end

  def delete_projects_companies(project_id)
    @@company_dao.delete_projects_companies(project_id)
  end
end