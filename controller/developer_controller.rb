require File.expand_path('dao/developer_dao')

class DeveloperController
  @@developer_dao = DeveloperDao.new

  def save(developer)
    @@developer_dao.save(developer)
  end

  def get_by_id(developer_id)
    @@developer_dao.get_by_id(developer_id)
  end

  def get_by_name(developer_name)
    @@developer_dao.get_by_name(developer_name)
  end

  def show_all
    @@developer_dao.show_all
  end

  def update(developer)
    @@developer_dao.update(developer)
  end

  def delete(developer_id)
    @@developer_dao.delete(developer_id)
  end

  def developers_skills(developer_id, skill_id)
    @@developer_dao.developers_skills(developer_id, skill_id)
  end

  def delete_developers_skills(developer_id)
    @@developer_dao.delete_developers_skills(developer_id)
  end

  def delete_skills_developers(skill_id)
    @@developer_dao.delete_skills_developers(skill_id)
  end

  def developers_projects(developer_id, project_id)
    @@developer_dao.developers_projects(developer_id, project_id)
  end

  def delete_developers_projects(developer_id)
    @@developer_dao.delete_developers_projects(developer_id)
  end

  def delete_projects_developers(project_id)
    @@developer_dao.delete_projects_developers(project_id)
  end

  def show_all_skills_by_dev_id(developer_id)
    @@developer_dao.show_all_skills_by_dev_id(developer_id)
  end

  def show_absent_skills(existing_skill_ids)
    @@developer_dao.show_absent_skills(existing_skill_ids)
  end

  def show_all_projects_by_dev_id(developer_id)
    @@developer_dao.show_all_projects_by_dev_id(developer_id)
  end

  def show_absent_projects(existing_project_ids)
    @@developer_dao.show_absent_projects(existing_project_ids)
  end
end
