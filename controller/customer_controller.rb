require './dao/customer_dao'

class CustomerController
  @@customer_dao = CustomerDao.new

  def save(customer)
    @@customer_dao.save(customer)
  end

  def get_by_id(customer_id)
    @@customer_dao.get_by_id(customer_id)
  end

  def get_by_name(customer_name)
    @@customer_dao.get_by_name(customer_name)
  end

  def show_all
    @@customer_dao.show_all
  end

  def update(customer)
    @@customer_dao.update(customer)
  end

  def delete(customer_id)
    @@customer_dao.delete(customer_id)
  end

  def delete_customer_projects(customer_id)
    @@customer_dao.delete_customer_projects(customer_id)
  end

  def customers_projects(customer_id, project_id)
    @@customer_dao.customers_projects(customer_id, project_id)
  end

  def show_all_projects_by_cus_id(customer_id)
    @@customer_dao.show_all_projects_by_cus_id(customer_id)
  end

  def show_absent_projects(existing_project_ids)
    @@customer_dao.show_absent_projects(existing_project_ids)
  end

  def delete_projects_customers(project_id)
    @@customer_dao.delete_projects_customers(project_id)
  end
end