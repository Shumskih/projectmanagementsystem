CREATE TABLE developers (id int PRIMARY KEY,
                          name VARCHAR(100),
                          surname VARCHAR(100),
                          specialization VARCHAR(100),
                          experience int,
                          salary int);

CREATE TABLE skills (id int, name VARCHAR(100), PRIMARY KEY (id));

CREATE TABLE companies (id int, name VARCHAR(100), PRIMARY KEY (id));

CREATE TABLE customers (id int, name VARCHAR(100), PRIMARY KEY (id));

CREATE TABLE projects (id int, name VARCHAR(100), version VARCHAR(100), cost int, PRIMARY KEY (id));

CREATE TABLE developers_skills (developer_id int, skill_id int,
                                FOREIGN KEY (developer_id) REFERENCES developers(id),
                                FOREIGN KEY (skill_id) REFERENCES skills(id));

CREATE TABLE developers_projects (developer_id int, project_id int,
                                  FOREIGN KEY (developer_id) REFERENCES developers(id),
                                  FOREIGN KEY (project_id) REFERENCES projects(id));

CREATE TABLE customers_projects (customer_id int, project_id int,
                                  FOREIGN KEY (customer_id) REFERENCES customers(id),
                                  FOREIGN KEY (project_id) REFERENCES projects(id));

CREATE TABLE companies_projects (company_id int, project_id int,
                                  FOREIGN KEY (company_id) REFERENCES companies(id),
                                  FOREIGN KEY (project_id) REFERENCES projects(id));

