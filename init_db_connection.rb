require 'pg'
require './config/database'

class InitDbConnection
  include Database

  @@init_db = InitDbConnection.new

  def self.init_db
    @@init_db
  end

  def db_connection
    @connection = PG.connect dbname: DB, port: 5432, user: DB_USER, password: DB_PASSWORD
  end

  def close_db_connection(connection)
    connection.close
  end

  private_class_method :new
end